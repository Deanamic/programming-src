#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef vector<int> vi;

#define FOR(i, a, b) for (int i = (a); i < int(b); i++)
#define FORD(i, a, b) for (int i = (b)-1; i >= int(a); i--)
#define sz(c) int((c).size())
#define all(c) c.begin(), c.end()
#define debug(x) cerr << #x << ": " << x << endl;

const string IMPOSSIBLE = "IMPOSSIBLE\n";

bool can(int curS, vector<int> u, int a, int b) {
  int tp_pt = sz(u)-1;
  priority_queue<pair<int,int>> pq;
  for(pq.push({curS, 1}); pq.size();) {
    int curVal = pq.top().first;
    if(tp_pt > curVal) return false;
    int cnt = 0;
    while(pq.size() && pq.top().first == curVal) {
      cnt += pq.top().second;
      pq.pop();
    }
    // debug(curVal);
    // debug(cnt);
    // debug(tp_pt);
    if(curVal < sz(u)) {
      if(u[curVal]) {
        int t = min(u[curVal], cnt);
        u[curVal] -= t;
        cnt -= t;
      }
      if (u[curVal] == 0) {
        if (curVal == tp_pt) {
          while (tp_pt >= 0 && u[tp_pt] == 0)
            --tp_pt;
        }
      }
    }
    if (cnt > 0) {
      if(curVal - a >= 0) pq.push({curVal - a, cnt});
      if(curVal - b >= 0) pq.push({curVal - b, cnt});
    }
    if(tp_pt == -1) return true;
  }
  return false;
}

void solve() {
  int n, a, b;
  cin >> n >> a >> b;
  int g = __gcd(a, b);
  int lst = -1;
  vector<int> u(n);
  for(int i = 0; i < n; ++i) {
    cin >> u[i];
  }
  for(int i = 0; i < n; ++i) {
    if (u[i]) {
      if (lst == -1) {
        lst = i;
      }
      if((i - lst) % g) {
        cout << IMPOSSIBLE;
        return;
      }
      lst = i;
    }
  }
  int i = 0;
  while(++i) {
    int curS = (n-1) + i*g;
    if(can(curS, u, a, b)) {
      cout << curS + 1  << '\n';
      return;
    }
  }
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  int T;
  cin >> T;
  for (int i = 1; i <= T; ++i) {
    cout << "Case #" << i << ": ";
    solve();
  }
}
