#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef vector<int> vi;

#define FOR(i, a, b) for (int i = (a); i < int(b); i++)
#define FORD(i, a, b) for (int i = (b)-1; i >= int(a); i--)
#define sz(c) int((c).size())
#define all(c) c.begin(), c.end()
#define debug(x) cerr << #x << ": " << x << endl;

int T, N, B;
// Number of B towers, number of B-1 towers, number of B-2 towers, height of <
// B-2 towers -> Expected value I can further get?
// let dp[zero][one][two][small] be the expected value achievable at this step
// I want to know from a position, which future is the best -> from large to
// small
ll dp[21][21][21][13];
int action[21][21][21][13][10];

ll pow10[15];

void precompute() {
  pow10[0] = 1;
  for (int i = 1; i < 15; ++i) {
    pow10[i] = pow10[i - 1] * 10;
  }

  for (int zero = N; zero >= 0; --zero) {
    for (int one = N - zero; one >= 0; --one) {
      for (int two = N - zero - one; two >= 0; --two) {
        for (int small = 12; small >= 0; --small) {
          dp[zero][one][two][small] = 0;
          for (ll digit = 0; digit < 10; ++digit) {
            if (zero + one + two == N && small) {
              break;
            }
            // Put one in top pos
            ll curBest = -1;
            if (one > 0) {
              ll val = dp[zero + 1][one - 1][two][small] + (digit * pow10[14]);
              if (val > curBest) {
                curBest = val;
                action[zero][one][two][small][digit] = 14;
              }
            }
            // Put one in top-1 pos
            if (two > 0) {
              ll val = dp[zero][one + 1][two - 1][small] + (digit * pow10[13]);
              if (val > curBest) {
                curBest = val;
                action[zero][one][two][small][digit] = 13;
              }
            }
            // Put one in small stack
            if (one + two + zero < N) {
              int nextTwo = (small == 12 ? two + 1 : two);
              int nextSmall = (small + 1) % 13;
              ll val =
                  dp[zero][one][nextTwo][nextSmall] + (digit * pow10[small]);
              if (val > curBest) {
                curBest = val;
                action[zero][one][two][small][digit] = small;
              }
            }
            dp[zero][one][two][small] += curBest / 10;
          }
        }
      }
    }
  }
  cerr << dp[0][0][0][0] << endl;
}

void solve() {
  vector<int> h(N, 0);
  int z, o, t, s;
  z = o = t = s = 0;
  for (int i = 0; i < N * B; ++i) {
    int d;
    cin >> d;
    int nxt = action[z][o][t][s][d];
    bool p = 0;
    for (int j = 0; !p && j < N; ++j) {
      if (h[j] == nxt) {
        cout << j + 1 << endl;
        h[j]++;
        if (h[j] == 15)
          z++, o--;
        else if (h[j] == 14)
          o++, t--;
        else if (h[j] == 13)
          t++, s = 0;
        else
          s++;
        p = 1;
        // cerr << z << ' ' << o << ' ' << t << ' ' << s << endl;
      }
    }
    assert(p);
  }
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  cin >> T >> N >> B;
  ll P;
  cin >> P;
  precompute();
  for (int i = 1; i <= T; ++i) {
    solve();
  }
  int exit;
  cin >> exit;
}
