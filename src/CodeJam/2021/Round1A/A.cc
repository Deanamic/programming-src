#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef vector<int> vi;

#define FOR(i, a, b) for (int i = (a); i < int(b); i++)
#define FORD(i, a, b) for (int i = (b)-1; i >= int(a); i--)
#define sz(c) int((c).size())
#define all(c) c.begin(), c.end()
#define debug(x) cerr << #x << ": " << x << endl;

void solve() {
  int ans = 0;
  int n;
  cin >> n;
  string s[n];
  for (int i = 0; i < n; ++i) {
    cin >> s[i];
  }
  for (int i = 1; i < n; ++i) {
    int prevSz = sz(s[i - 1]);
    int curSz = sz(s[i]);
    if (curSz > prevSz) {
      continue;
    } else if (curSz == prevSz) {
      if (s[i] == s[i - 1]) {
        s[i] += '0';
      } else {
        for (int j = 0; j < curSz; ++j) {
          if (s[i][j] != s[i - 1][j]) {
            if (s[i][j] > s[i - 1][j])
              break;
            else {
              s[i] += '0';
              break;
            }
          }
        }
      }
    } else {
      bool fill0 = false;
      bool extra_digit = false;
      for (int j = 0; j < curSz; ++j) {
        if (s[i][j] != s[i - 1][j]) {
          if (s[i][j] > s[i - 1][j]) {
            fill0 = true;
            break;
          } else {
            extra_digit = true;
            break;
          }
        }
      }
      if (fill0) {
        s[i] += string(prevSz - curSz, '0');
      } else if (extra_digit) {
        s[i] += string(prevSz - curSz + 1, '0');
      } else {
        int lastNot9 = -1;
        for (int j = curSz; j < prevSz; ++j) {
          if (s[i - 1][j] != '9') {
            lastNot9 = j;
          }
        }
        if (lastNot9 == -1) {
          s[i] += string(prevSz - curSz + 1, '0');
        } else {
          for (int j = curSz; j < prevSz; ++j) {
            if (j < lastNot9) {
              s[i] += s[i - 1][j];
            } else if (j == lastNot9) {
              s[i] += s[i - 1][j] + 1;
            } else {
              s[i] += '0';
            }
          }
        }
      }
    }
    ans += sz(s[i]) - curSz;
  }
  cout << ans << '\n';
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  int T;
  cin >> T;
  for (int i = 1; i <= T; ++i) {
    cout << "Case #" << i << ": ";
    solve();
  }
}
