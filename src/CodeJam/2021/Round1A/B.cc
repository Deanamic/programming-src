#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef vector<int> vi;

#define FOR(i, a, b) for (int i = (a); i < int(b); i++)
#define FORD(i, a, b) for (int i = (b)-1; i >= int(a); i--)
#define sz(c) int((c).size())
#define all(c) c.begin(), c.end()
#define debug(x) cerr << #x << ": " << x << endl;

long long binpow(long long a, long long b, ll lim) {
  long long res = 1;
  while (b > 0) {
    if (b & 1)
      res = res * a;
    a = a * a;
    b >>= 1;
    if(res > lim) return -1;
  }
  return res;
}

ll solve() {
  int n;
  cin >> n;
  ll sum = 0;
  vector<ll> p(n), x(n);
  for (int i = 0; i < n; ++i) {
    cin >> p[i] >> x[i];
    sum += p[i] * x[i];
  }
  for (ll C = sum; C > max(0LL, sum - 120); --C) {
    ll curC = C;
    ll curSum = sum;
    for(int i = 0; i < n; ++i) {
      ll curX = x[i];
      ll curP = p[i];
      while (curX > 0 && curC % curP == 0) {
        curX--;
        curC /= curP;
        curSum -= curP;
      }
    }
    if (curC == 1 && curSum == C) {
      return C;
    }
  }
  return 0;
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  int T;
  cin >> T;
  for (int i = 1; i <= T; ++i) {
    cout << "Case #" << i << ": " << solve() << '\n';
  }
}
