#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef vector<int> vi;

#define FOR(i, a, b) for (int i = (a); i < int(b); i++)
#define FORD(i, a, b) for (int i = (b)-1; i >= int(a); i--)
#define sz(c) int((c).size())
#define all(c) c.begin(), c.end()
#define debug(x) cerr << #x << ": " << x << endl;

void flip(string& s, int& n, int q) {
  for (int i = 0; i < q; ++i) {
    s[i] = (s[i] == 'F' ? 'T' : 'F');
  }
  n = q - n;
}

void solveHard() {
  int q;
  cin >> q;
  string s1, s2, s3;
  int n1, n2, n3;
  cin >> s1 >> n1;
  cin >> s2 >> n2;
  cin >> s1 >> n3;
  return;
}
void solveEasy() {
  int q;
  cin >> q;
  string s1, s2;
  int n1, n2;
  cin >> s1 >> n1;
  cin >> s2 >> n2;
  if(n1 < q - n1) {
    flip(s1, n1, q);
  }
  if (n2 < q - n2) {
    flip(s2, n2, q);
  }
  int common = 0;
  for(int i = 0; i < q; ++i) {
    if(s1[i] == s2[i]) ++common;
  }
  int diff = q - common;
  int commonCorrect = (max(n1, n2) - diff/2);
  if(common - commonCorrect > commonCorrect) {
    n1 = n1 - 2 * commonCorrect + common;
    n2 = n2 - 2 * commonCorrect + common;
    for (int i = 0; i < q; ++i) {
      if (s1[i] == s2[i]) {
        s1[i] = (s1[i] == 'F' ? 'T' : 'F');
        s2[i] = (s2[i] == 'F' ? 'T' : 'F');
      }
    }
  }
  if(n1 > n2) {
    cout << s1 << ' ' << n1 << "/1\n";
  } else {
    cout << s2 << ' ' << n2 << "/1\n";
  }
}

void solve() {
  int n;
  cin >> n;
  if (n == 1) {
    int q;
    cin >> q;
    string s1;
    int n1;
    cin >> s1 >> n1;
    if(q - n1 > n1) {
      for (int i = 0; i < sz(s1); ++i) {
        s1[i] = (s1[i] == 'F' ? 'T' : 'F');
      }
      n1 = q - n1;
    }
    cout << s1 << ' ' << n1 << "/1\n";
  } else if (n == 2){
    solveEasy();
  } else {
    solveHard();
  }
}
int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  int T;
  cin >> T;
  for (int i = 1; i <= T; ++i) {
    cout << "Case #" << i << ": ";
    solve();
  }
}
