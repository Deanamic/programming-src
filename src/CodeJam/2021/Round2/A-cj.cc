#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef vector<int> vi;

#define FOR(i, a, b) for (int i = (a); i < int(b); i++)
#define FORD(i, a, b) for (int i = (b)-1; i >= int(a); i--)
#define sz(c) int((c).size())
#define all(c) c.begin(), c.end()
#define debug(x) cerr << #x << ": " << x << endl;

void solve() {
  for (int i = 1; i <= 99; ++i) {
    cout << "M " << i << ' ' << 100 << endl;
    int id;
    cin >> id;
    if (i != id) {
      cout << "S " << i << ' '<< id << endl;
      int rip;
      cin >> rip;
    }
  }
  cout << "D" << endl;
  int exit;
  cin >> exit;
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  int T;
  int n;
  cin >> T >> n;
  for (int i = 1; i <= T; ++i) {
    solve();
  }
}
