#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef vector<int> vi;

#define FOR(i, a, b) for (int i = (a); i < int(b); i++)
#define FORD(i, a, b) for (int i = (b)-1; i >= int(a); i--)
#define sz(c) int((c).size())
#define all(c) c.begin(), c.end()
#define debug(x) cerr << #x << ": " << x << endl;

typedef int value_type;
value_type MinAssign(const vector<vector<value_type>> &c) {
    const int n = c.size(), m = c[0].size(); // assert(n <= m);
    vector<value_type> v(m), dist(m);        // v: potential
    vector<int> matchL(n,-1), matchR(m,-1);  // matching pairs
    vector<int> index(m), prev(m);
    iota(all(index), 0);
    auto residue = [&](int i, int j) { return c[i][j] - v[j]; };
    for (int f = 0; f < n; ++f) {
        for (int j = 0; j < m; ++j) {
            dist[j] = residue(f, j);
            prev[j] = f;
        }
        value_type w;
        int j, l;
        bool end = 0;
        for (int s = 0, t = 0;;) {
            if (s == t) {
                l = s; w = dist[index[t++]];
                for (int k = t; k < m; ++k) {
                    j = index[k];
                    value_type h = dist[j];
                    if (h <= w) {
                        if (h < w) { t = s; w = h; }
                        index[k] = index[t]; index[t++] = j;
                    }
                }
                for (int k = s; k < t; ++k) {
                    j = index[k];
                    if (matchR[j] < 0) {
                        end = 1;
                        break;
                    }
                }
                if(end) break;
            }
            int q = index[s++], i = matchR[q];
            for (int k = t; k < m; ++k) {
                j = index[k];
                value_type h = residue(i,j) - residue(i,q) + w;
                if (h < dist[j]) {
                    dist[j] = h; prev[j] = i;
                    if (h == w) {
                        if (matchR[j] < 0) {
                            end = 1;
                            break;
                        }
                        index[k] = index[t]; index[t++] = j;
                    }
                }
            }
            if(end) break;
        }
        for(int k = 0; k < l; ++k)
            v[index[k]] += dist[index[k]] - w;
        int i;
        do {
            matchR[j] = i = prev[j];
            swap(j, matchL[i]);
        } while (i != f);
    }
    value_type opt = 0;
    for (int i = 0; i < n; ++i)
        opt += c[i][matchL[i]]; // (i, matchL[i]) is a solution
    return opt;
}

void solve() {
  int r, c, f, s;
  cin >> r >> c >> f >> s;
  f = 2*f;
  vector<pair<int, int>> posG, posM;
  vector<vector<int>> value(r * c, vector<int>(2 * r * c, 1e9));
  for (int i = 0; i < r; ++i) {
    for (int j = 0; j < c; ++j) {
      char l;
      cin >> l;
      if (l == 'G') {
        posG.push_back({i, j});
      } else {
        posM.push_back({i, j});
      }
    }
  }
  for (int i = 0; i < r; ++i) {
    for (int j = 0; j < c; ++j) {
      char l;
      cin >> l;
      if (l == 'G') {
        for (auto p : posG) {
          int S = abs(p.first - i) + abs(p.second - j);
          S *= s;
          value[i * c + j][p.first * c + p.second] = S;
        }
        value[i * c + j][i * c + j + r * c] = f;
      } else {
        for (auto p : posM) {
          int S = abs(p.first - i) + abs(p.second - j);
          S *= s;
          value[i * c + j][p.first * c + p.second] = S;
        }
        value[i * c + j][i * c + j + r * c] = f;
      }
    }
  }
  cout << MinAssign(value)/2 << endl;
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  int T;
  cin >> T;
  for (int i = 1; i <= T; ++i) {
    cout << "Case #" << i << ": ";
    solve();
  }
}
