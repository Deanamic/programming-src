#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef vector<int> vi;

#define FOR(i, a, b) for (int i = (a); i < int(b); i++)
#define FORD(i, a, b) for (int i = (b)-1; i >= int(a); i--)
#define sz(c) int((c).size())
#define all(c) c.begin(), c.end()
#define debug(x) cerr << #x << ": " << x << endl;

vi memo(1000010, -1);

int td(int n) {
  if (memo[n] != -1) {
    return memo[n];
  }
  memo[n] = 1;
  for (int i = 2; i * i <= n; ++i) {
    if (n % i == 0) {
      memo[n] = max(memo[n], 1 + td(n / i - 1));
    }
  }
  return memo[n];
}

void solve() {
  int n;
  cin >> n;
  int ans = 1;
  for (int i = 3; i * i <= n; ++i) {
    if (n % i == 0) {
      ans = max(ans, 1 + td(n / i - 1));
      ans = max(ans, 1 + td(i - 1));
    }
  }
  cout << ans << endl;
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  memo[0] = 0;
  memo[1] = 0;
  int T;
  cin >> T;
  for (int i = 1; i <= T; ++i) {
    cout << "Case #" << i << ": ";
    solve();
  }
}
