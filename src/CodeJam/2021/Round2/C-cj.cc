#include <bits/stdc++.h>
using namespace std;

using ll = long long;
using vi = vector<int>;

#define FOR(i, a, b) for (int i = (a); i < int(b); i++)
#define FORD(i, a, b) for (int i = (b)-1; i >= int(a); i--)
#define sz(c) int((c).size())
#define all(c) c.begin(), c.end()
#define debug(x) cerr << #x << ": " << x << endl;

const int MOD = 1000000007;
const int MAX_N = 100010;

vector<int> v;
ll fact[MAX_N];

void precompute() {
  fact[0] = 1;
  for (ll i = 1; i < 100005; ++i) {
    fact[i] = fact[i - 1] * i % MOD;
  };
}

struct SparseTable {
  // ST saves the index, not the value
  vector<vector<int>> ST;
  vector<int> P;
  int N;
  int MAXLOG = 0;
  void build(int n, const vector<int> &V) {
    N = n;
    MAXLOG = 32 - __builtin_clz(N);
    ST = vector<vector<int>>(N, vector<int>(MAXLOG));
    // Dynamic Sparse table building
    for (int i = 0; i < N; ++i) {
      ST[i][0] = i;
    }
    for (int j = 1; j < MAXLOG; ++j) {
      for (int i = 0; i + (1 << j) - 1 < N; ++i) {
        if (V[ST[i][j - 1]] < V[ST[i + (1 << (j - 1))][j - 1]])
          ST[i][j] = ST[i][j - 1];
        else
          ST[i][j] = ST[i + (1 << (j - 1))][j - 1];
      }
    }
  }

  //[l,r] range
  int query(int l, int r, const vector<int> &V) {
    int LOG = 31 - __builtin_clz(r - l + 1);
    if (V[ST[l][LOG]] < V[ST[r - (1 << LOG) + 1][LOG]]) {
      return ST[l][LOG];
    }
    return ST[r - (1 << LOG) + 1][LOG];
  }
} S;

ll binpow(ll a, ll b) {
  a %= MOD;
  ll res = 1;
  while (b > 0) {
    if (b & 1) {
      res = res * a % MOD;
    }
    a = a * a % MOD;
    b >>= 1;
  }
  return res;
}

ll inverse(ll x) { return binpow(x, MOD - 2); }

ll comb(int n, int k) {
  return fact[n] * inverse(fact[k] * fact[n - k] % MOD) % MOD;
}

ll rec(int l, int r, int off) {
  if (l > r) {
    return 1;
  }
  if (l == r) {
    return (off == v[l]);
  }
  int m = S.query(l, r, v);
  if (v[m] != off) {
    return 0;
  }
  return rec(l, m - 1, off) * rec(m + 1, r, off + 1) % MOD *
         comb(r - l, m - l) % MOD;
}

void solve() {
  int n;
  cin >> n;
  v = vector<int>(n);
  for (int i = 0; i < n; ++i) {
    cin >> v[i];
  }
  S.build(n, v);
  cout << rec(0, n - 1, 1) << endl;
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  precompute();
  int T;
  cin >> T;
  for (int i = 1; i <= T; ++i) {
    cout << "Case #" << i << ": ";
    solve();
  }
}
