#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef vector<int> vi;

#define FOR(i, a, b) for (int i = (a); i < int(b); i++)
#define FORD(i, a, b) for (int i = (b)-1; i >= int(a); i--)
#define sz(c) int((c).size())
#define all(c) c.begin(), c.end()
#define debug(x) cerr << #x << ": " << x << endl;

set<int> elements;

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  // Pick (1,x)
  // All the elements larger than x should not be together
  // We append elements in order reversely (yi, zi), y > x
  // Now all elements larger than
  int n;
  cin >> n;
  map<int, pair<int, int>> cards;
  for (int i = 0; i < n; ++i) {
    int l, r;
    cin >> l >> r;
    cards[l] = {l, r};
    cards[r] = {l, r};
  }
  for (int i = 0; i < 2 * n; ++i) {
    elements.insert(i + 1);
  }
  // get card one
  int ans = 0;
  stack<int> lft_bot, lft_top, rgt_bot, rgt_top;
  while (sz(elements)) {
    auto p = cards[*elements.begin()];
    bool shouldSwap = (p.first != *elements.begin());
    if (shouldSwap) {
      swap(p.first, p.second);
    }
    lft_bot.push(p.first);
    elements.erase(p.first);
    rgt_bot.push(p.second);
    elements.erase(p.second);

    int cost = 0;
    int num = 1;
    bool activate = 1;
    while (activate) {
      activate = 0;
      // insert larger than right_bot stack to the left_top
      if (!sz(elements)) {
        break;
      }
      for (auto i = --elements.end();sz(rgt_bot) && *i > rgt_bot.top(); i = --elements.end()) {
        activate = 1;
        num++;
        auto it = *i;
        auto t = cards[it];
        if (shouldSwap) {
          swap(t.first, t.second);
        }
        if (t.first != it) {
          swap(t.first, t.second);
          ++cost;
        }
        if (sz(lft_top) && t.first > lft_top.top()) {
          return cout << -1 << endl, 0;
        }
        lft_top.push(t.first);
        elements.erase(t.first);

        if (sz(rgt_top) && t.second < rgt_top.top()) {
          return cout << -1 << endl, 0;
        }
        rgt_top.push(t.second);
        elements.erase(t.second);
        if (!sz(elements)) {
          break;
        }
      }
      if (sz(rgt_top) && sz(rgt_bot) && rgt_top.top() > rgt_bot.top()) {
        return cout << -1 << endl, 0;
      }
      if (sz(lft_top) && sz(lft_bot) && lft_top.top() < lft_bot.top()) {
        return cout << -1 << endl, 0;
      }
      if (!sz(elements)) {
        break;
      }
      // insert smaller than right_top stack to the left_bot
      for (auto i = elements.begin();sz(rgt_top) && *i < rgt_top.top(); i = elements.begin()) {
        activate = 1;
        num++;
        auto it = *i;
        auto t = cards[it];
        if (shouldSwap) {
          swap(t.first, t.second);
        }
        if (t.first != it) {
          swap(t.first, t.second);
          ++cost;
        }
        if (sz(lft_bot) && t.first < lft_bot.top()) {
          return cout << -1 << endl, 0;
        }
        lft_bot.push(t.first);
        elements.erase(t.first);

        if (sz(rgt_bot) && t.second > rgt_bot.top()) {
          return cout << -1 << endl, 0;
        }
        rgt_bot.push(t.second);
        elements.erase(t.second);
        if (!sz(elements)) {
          break;
        }
      }
      if (sz(rgt_top) && sz(rgt_bot) && rgt_top.top() > rgt_bot.top()) {
        return cout << -1 << endl, 0;
      }
      if (sz(lft_top) && sz(lft_bot) && lft_top.top() < lft_bot.top()) {
        return cout << -1 << endl, 0;
      }
      if (!sz(elements)) {
        break;
      }
    }
    ans += min(cost, num - cost);
  }
  cout << ans << endl;
}
