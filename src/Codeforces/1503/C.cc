#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef vector<int> vi;

#define FOR(i, a, b) for (int i = (a); i < int(b); i++)
#define FORD(i, a, b) for (int i = (b)-1; i >= int(a); i--)
#define sz(c) int((c).size())
#define all(c) c.begin(), c.end()
#define debug(x) cerr << #x << ": " << x << endl;

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  int n;
  cin >> n;
  pair<int, int> p[n];
  ll ans = 0;
  for (int i = 0; i < n; ++i) {
    cin >> p[i].first >> p[i].second;
    ans += p[i].second;
  }
  pair<int, int> ref = p[0];
  int bd = p[0].first + p[0].second;
  sort(p, p + n);
  int st = 0;
  int bd0 = p[0].first + p[0].second;
  while (p[st] != ref) {
    ans += max(0, p[st].first - bd0);
    bd0 = max(bd0, p[st].first + p[st].second);
    ++st;
  }
  bd = max(bd, bd0);
  if(st) ans += max(0, p[st].first - bd0);
  while (st < n) {
    ans += max(0, p[st].first - bd);
    bd = max(bd, p[st].first + p[st].second);
    st++;
  }
  cout << ans << endl;
}
