#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef vector<int> vi;

#define FOR(i, a, b) for (int i = (a); i < int(b); i++)
#define FORD(i, a, b) for (int i = (b)-1; i >= int(a); i--)
#define sz(c) int((c).size())
#define all(c) c.begin(), c.end()
#define debug(x) cerr << #x << ": " << x << endl;

// 1 1  1 3
int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  int n;
  cin >> n;

  int ox = 1, oy = 1;
  int px = 1, py = 2;
  int oWin = 0;
  int pWin = 0;
  int i = 0;
  for (; i < n * n; ++i) {
    int c;
    cin >> c;
    if (c == 1) {
      cout << 2 << ' ' << px << ' ' << py << endl;
      py += 2;
      if (py > n) {
        ++px;
        py = 1 + (px%2);
      }
      if (px > n) {
        pWin = 1;
        break;
      }
    } else {
      cout << 1 << ' ' << ox << ' ' << oy << endl;
      oy += 2;
      if (oy > n) {
        ++ox;
        oy = 2 - (ox % 2);
      }
      if (ox > n) {
        oWin = 1;
        break;
      }
    }
  }
  for (; i < n * n; ++i) {
    int c;
    cin >> c;
    if (oWin) {
      if (c == 3) {
        cout << 2 << ' ' << px << ' ' << py << endl;
      } else {
        cout << 3 << ' ' << px << ' ' << py << endl;
      }
      py += 2;
      if (py > n) {
        ++px;
        py = 1 + (px % 2);
      }
      if (px > n) {
        pWin = 1;
        break;
      }
    } else if (pWin) {
      if (c == 3) {
        cout << 1 << ' ' << ox << ' ' << oy << endl;
      } else {
        cout << 3 << ' ' << ox << ' ' << oy << endl;
      }
      oy += 2;
      if (oy > n) {
        ++ox;
        oy = 2 - (ox % 2);
      }
      if (ox > n) {
        oWin = 1;
        break;
      }
    }
  }
}
