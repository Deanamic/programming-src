#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef vector<int> vi;

#define FOR(i, a, b) for (int i = (a); i < int(b); i++)
#define FORD(i, a, b) for (int i = (b)-1; i >= int(a); i--)
#define sz(c) int((c).size())
#define all(c) c.begin(), c.end()
#define debug(x) cerr << #x << ": " << x << endl;

void solve() {
  int n;
  cin >> n;
  string s;
  cin >> s;
  int n0 = 0, n1 = 0;
  for (char c : s)
    (c == '0' ? ++n0 : ++n1);
  if ((n0 & 1) || (n1 & 1)) {
    cout << "NO\n";
    return;
  }
  string s0 = "", s1 = "";
  int open = n1 / 2;
  int val0 = 0, val1 = 0;
  int last = 0;
  for (char c : s) {
    if(c == '0') {
      if(last) {
        s0 += '(';
        s1 += ')';
        val0++;
        val1--;
      } else {
        s0 += ')';
        s1 += '(';
        val0--;
        val1++;
      }
      last = 1 - last;
    } else {
      if(open > 0) {
        s0 += '(';
        s1 += '(';
        val0++;
        val1++;
        --open;
      } else {
        s0 += ')';
        s1 += ')';
        val0--;
        val1--;
      }
    }
    if(val1 < 0 || val0 < 0) {
      cout << "NO\n";
      return;
    }
  }
  cout << "YES\n" << s0 << '\n' << s1 << '\n';
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  int T;
  cin >> T;
  while (T--)
    solve();
}
