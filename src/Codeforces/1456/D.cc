#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef vector<int> vi;

#define FOR(i, a, b) for (int i = (a); i < int(b); i++)
#define FORD(i, a, b) for (int i = (b)-1; i >= int(a); i--)
#define sz(c) int((c).size())
#define all(c) c.begin(), c.end()
#define debug(x) cerr << #x << ": " << x << endl;

struct Events {
  ll t;
  ll x;
};

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  int n;
  cin >> n;
  vector<Events> v(n);
  for (int i = 0; i < n; ++i) {
    cin >> v[i].t >> v[i].x;
  }
  vector<vector<ll>> dp(n, vector<ll>(n, 1e15));
  for (int i = 0; i < n; ++i) {
    dp[0][i] = abs(v[i].x) + abs(v[0].x - v[i].x);
  }
  for (int i = 1; i < n; ++i) {
    for (int j = 0; j < n; ++j) {
      // When the optimum is taking the clone from a previous moment
      if (dp[i - 1][j] <= v[i - 1].t) {
        dp[i][j] = min(dp[i][j], v[i - 1].t + abs(v[i].x - v[i - 1].x));
      }
      // when the optimum is taking the clone coming from the last time
      if (dp[i - 1][i - 1] <= v[i - 1].t) {
        dp[i][j] = min(dp[i][j], max(v[i - 1].t, dp[i - 1][i - 1] +
                                                     abs(v[i - 1].x - v[j].x)) +
                                     abs(v[i].x - v[j].x));
      }
      // when we can skip the last place because we had a clone
      if (i - 2 >= 0) {
        if (dp[i - 2][i - 1] <= v[i - 2].t) {
          dp[i][j] = min(
              dp[i][j], max(v[i - 1].t, v[i - 2].t + abs(v[i - 2].x - v[j].x)) +
                            abs(v[j].x - v[i].x));
        }
      }
    }
  }
  // for (int i = 0; i < n; ++i) {
  //   for (int j = 0; j < n; ++j) {
  //     cout << dp[i][j] << ' ';
  //   }
  //   cout << endl;
  // }
  cout << ((n - 2 && dp[n - 2][n - 1] <= v[n - 2].t) ||
                   dp[n - 1][n - 1] <= v[n - 1].t
               ? "YES"
               : "NO")
       << endl;
}
