#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef vector<int> vi;

#define FOR(i, a, b) for (int i = (a); i < int(b); i++)
#define FORD(i, a, b) for (int i = (b)-1; i >= int(a); i--)
#define sz(c) int((c).size())
#define all(c) c.begin(), c.end()
#define debug(x) cerr << #x << ": " << x << endl;

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  int n, k;
  cin >> n >> k;
  int c[n];
  priority_queue<ll> q;
  for (int i = 0; i < n; ++i) {
    cin >> c[i];
  }
  for (int i = 0; i <= k; ++i) {
    q.push(0);
  }
  sort(c, c + n, [](int a, int b) { return a > b; });
  ll ans = 0;
  for(int i = 0; i < n; ++i) {
    ll t = q.top();
    q.pop();
    ans += t;
    q.push(t + c[i]);
  }
  cout << ans << endl;
}
