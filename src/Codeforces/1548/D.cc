#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef vector<int> vi;

#define FOR(i, a, b) for (int i = (a); i < int(b); i++)
#define FORD(i, a, b) for (int i = (b)-1; i >= int(a); i--)
#define sz(c) int((c).size())
#define all(c) c.begin(), c.end()
#define debug(x) cerr << #x << ": " << x << endl;

struct SparseTable {
  vector<vector<ll>> ST;
  int N;
  int MAXLOG = 0;

  void build(int n, const vector<ll> &V) {
    N = n;
    while ((1 << MAXLOG) <= N)
      ++MAXLOG;
    ST = vector<vector<ll>>(N, vector<ll>(MAXLOG));

    for (int i = 0; i < N; ++i)
      ST[i][0] = V[i];
    for (int j = 1; j < MAXLOG; ++j) {
      for (int i = 0; i + (1 << j) - 1 < N; ++i) {
        ST[i][j] = __gcd(ST[i][j - 1], ST[i + (1 << (j - 1))][j - 1]);
      }
    }
  }

  // minimum in range [l, r] (both inclusive)
  ll query(int l, int r) {
    int LOG = 31 - __builtin_clz(r - l + 1);
    return __gcd(ST[l][LOG], ST[r - (1 << LOG) + 1][LOG]);
  }
};

void solve() {
  int n;
  cin >> n;
  vector<ll> v(n);
  for (int i = 0; i < n; ++i) {
    cin >> v[i];
    if (i) {
      v[i - 1] = abs(v[i] - v[i - 1]);
    }
  }
  SparseTable S;
  S.build(n - 1, v);
  int it1 = 0, it2 = 0;
  int curAns = 0;
  ll curGcd = v[0];
  while (it2 < n - 1) {
    curGcd = S.query(it1, it2);
    ++it2;
    if (curGcd > 1) {
      ++curAns;
    } else {
      ++it1;
    }
  }
  cout << curAns + 1 << '\n';
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  int T;
  cin >> T;
  while (T--) {
    solve();
  }
}
