#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef vector<int> vi;

#define FOR(i, a, b) for (int i = (a); i < int(b); i++)
#define FORD(i, a, b) for (int i = (b)-1; i >= int(a); i--)
#define sz(c) int((c).size())
#define all(c) c.begin(), c.end()
#define debug(x) cerr << #x << ": " << x << endl;

struct song {
  int val;
  int prevId, nextId;
  int safe;
};

void solve() {
  int n;
  cin >> n;
  vector<song> v(n);
  for (int i = 0; i < n; ++i) {
    int x;
    cin >> x;
    v[i] = song{x, (i - 1 + n) % n, (i + 1 + n) % n, 0};
  }
  queue<int> q, unsafeQueue;
  for (int i = 0; i < n; ++i) {
    unsafeQueue.push((i + 1 + n) % n);
  }
  while(sz(unsafeQueue)) {
    int it = unsafeQueue.front();
    unsafeQueue.pop();
    int prevId = v[it].prevId;
    int nextId = v[it].nextId;
    if (__gcd(v[it].val, v[prevId].val) == 1) {
      q.push(it);
      v[prevId].nextId = nextId;
      v[nextId].prevId = prevId;
      if(v[nextId].safe) {
        unsafeQueue.push(nextId);
        v[nextId].safe = 0;
      }
      else if(nextId == unsafeQueue.front()) {
        unsafeQueue.push(nextId);
        unsafeQueue.pop();
      }
    }
    else {
      v[it].safe = 1;
    }
  }
  cout << sz(q);
  for (; sz(q); q.pop()) {
    int t = q.front();
    cout << ' ' << t + 1;
  }
  cout << '\n';
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  int t;
  cin >> t;
  while (t--)
    solve();
}
