#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef vector<int> vi;

#define FOR(i, a, b) for (int i = (a); i < int(b); i++)
#define FORD(i, a, b) for (int i = (b)-1; i >= int(a); i--)
#define sz(c) int((c).size())
#define all(c) c.begin(), c.end()
#define debug(x) cerr << #x << ": " << x << endl;

void solve() {
  int n, m;
  cin >> n >> m;
  cerr << n << ' ' << m << endl;
  vector<vi> v(m);
  vi ans(m);
  vi cnt(n, 0), ass(n, 0);
  int ref = -1;
  int rip = 0;
  for (int i = 0; i < m; ++i) {
    int x;
    cin >> x;
    for (int j = 0; j < x; ++j) {
      int y;
      cin >> y;
      v[i].push_back(--y);
      if (cnt[y]++ == (m + 1) / 2) {
        if (ref == -1)
          ref = y;
        else
          ref = -2;
      }
    }
    if (x == 1) {
      ++ass[v[i][0]];
      ans[i] = v[i][0];
      if (ass[v[i][0]] > (m + 1) / 2) {
        if(!rip) cout << "NO\n";
        rip = 1;
      }
    }
  }
  if(rip) return;
  for (int i = 0; i < m; ++i) {
    if (sz(v[i]) > 1) {
      for (int j : v[i]) {
        if (ref == -2) {
          if (ass[j] < (m + 1) / 2) {
            ans[i] = j;
            ++ass[j];
            break;
          }
        } else if (j != ref) {
          ans[i] = j;
          if (ass[j]++ == (m + 1) / 2) {
            cout << "NOSEPASS\n";
            return;
          }
          break;
        }
      }
    }
  }
  cout << "YES\n";
  for (int i : ans)
    cout << i + 1 << ' ';
  cout << '\n';
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  int T;
  cin >> T;
  for (auto i = 0; i < T; ++i) {
    solve();
  }
}
