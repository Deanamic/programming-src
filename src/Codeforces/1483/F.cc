#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef vector<int> vi;

#define FOR(i, a, b) for (int i = (a); i < int(b); i++)
#define FORD(i, a, b) for (int i = (b)-1; i >= int(a); i--)
#define sz(c) int((c).size())
#define all(c) c.begin(), c.end()
#define debug(x) cerr << #x << ": " << x << endl;

const int MaxM = 1000005;

struct Trie {
  static const int Alpha = 26;
  static const int first = 'a';
  int lst = 1;
  struct node {
    int nxt[Alpha] = {}, p = -1;
    char c;
    int end = -1; // if all patterns are different, can use flag instead
    int SuffixLink = -1;
  };
  vector<node> V;
  vi ends, lens;
  int num;
  inline int getval(char c) { return c - first; }
  void CreateSuffixLink() {
    queue<int> q;
    for (q.push(0); q.size(); q.pop()) {
      int pos = q.front();
      if (!pos || !V[pos].p)
        V[pos].SuffixLink = 0;
      else {
        int val = getval(V[pos].c);
        int j = V[V[pos].p].SuffixLink;
        V[pos].SuffixLink = V[j].nxt[val];
      }
      for (int i = 0; i < Alpha; ++i) {
        if (V[pos].nxt[i])
          q.push(V[pos].nxt[i]);
        else if (!pos || !V[pos].p)
          V[pos].nxt[i] = V[0].nxt[i];
        else
          V[pos].nxt[i] = V[V[pos].SuffixLink].nxt[i];
      }
    }
  }

  void init() {
    V.resize(MaxM);
    cin >> num;
    for (int i = 0; i < num; ++i) {
      string s;
      cin >> s;
      int pos = 0;
      for (char c : s) {
        int val = getval(c);
        if (!V[pos].nxt[val]) {
          V[lst].p = pos;
          V[lst].c = c;
          V[pos].nxt[val] = lst++;
        }
        pos = V[pos].nxt[val];
      }
      lens.push_back(sz(s));
      ends.push_back(pos);
      V[pos].end = i;
    }
    CreateSuffixLink();
  }

  void Compress() {
    FOR(i, 0, lst) {
      while (V[i].SuffixLink > 0 && V[V[i].SuffixLink].end == -1) {
        V[i].SuffixLink = V[V[i].SuffixLink].SuffixLink;
      }
    }
  }

  int find() {
    Compress();
    int ans = 0;
    for (int i = 0; i < num; ++i) {
      int x = find(i);
      ans += x;
    }
    return ans;
  }

  inline void badPosition(int curP, unordered_set<int>& s,
                          unordered_set<int>& ban) {
    for (; curP != 0; curP = V[curP].SuffixLink) {
      if (V[curP].end > -1) {
        if (s.count(V[curP].end)) {
          s.erase(V[curP].end);
        }
        if (!ban.count(V[curP].end)) {
          ban.insert(V[curP].end);
        }
      }
    }
  }

  inline int find(int i) {
    int leftBorder = MaxM;
    unordered_set<int> s, ban;
    for (int pos = ends[i], len = lens[i]; pos != 0; pos = V[pos].p, --len) {
      for (int curP = pos; curP != 0; curP = V[curP].SuffixLink) {
        if (V[curP].end > -1 && V[curP].end != i) {
          if (ban.count(V[curP].end)) {
            break;
          }
          if (len - lens[V[curP].end] < leftBorder) {
            s.insert(V[curP].end);
            leftBorder = len - lens[V[curP].end];
            curP = V[curP].SuffixLink;
          }
          badPosition(curP, s, ban);
          break;
        }
      }
    }
    return sz(s);
  }
};

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  Trie AhoCorasick;
  AhoCorasick.init();
  cout << AhoCorasick.find() << endl;
}
