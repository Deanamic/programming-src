#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef vector<int> vi;

const int oo = 0x3f3f3f3f;

#define FOR(i, a, b) for (int i = (a); i < int(b); i++)
#define FORD(i, a, b) for (int i = (b)-1; i >= int(a); i--)
#define sz(c) int((c).size())
#define all(c) c.begin(), c.end()
#define debug(x) cerr << #x << ": " << x << endl;

struct Edge {
  int u, v, w;
  bool useful = 0;
} E[300 * 599];

inline void solve() {
  int n, m;
  cin >> n >> m;
  vector<vi> d(n, vi(n, oo));
  vector<int> adj[n];
  for (int i = 0; i < m; ++i) {
    int u, v, w;
    cin >> u >> v >> w;
    --u, --v;
    E[i] = {u, v, w};
    adj[u].push_back(i);
    adj[v].push_back(i);
    d[u][v] = w;
    d[v][u] = w;
  }
  // O(n^3): Floyd Warshall
  FOR(i, 0, n) d[i][i] = 0;
  FOR(k, 0, n) FOR(i, 0, n) FOR(j, 0, n) {
    d[i][j] = min(d[i][j], d[i][k] + d[k][j]);
  }
  int Q;
  cin >> Q;
  vector<pair<int, int>> queries[n];
  int cnt = 0;
  FOR(i, 0, Q) {
    int u, v, l;
    cin >> u >> v >> l;
    --u, --v;
    queries[u].emplace_back(v, l);
  }
  // O(n*T(n)) outer loop -> O(n*(nlogn + m)) = O(n^2logn + n^3)
  // Edge (u,v,w) is useful if the is a query (a,b,l) that fulfils: d(a,u) +
  // d(v,b) + w <= l Lets suppose we have only one possible value for b, and
  // try and solve in O(n^3)

  // Edge (u,v,w) is useful if the is a query (ai,b,li) that fulfils: d(ai,u)
  // + d(v,b) + w <= li Splitting by variables: d(ai, u) - li <= -d(v,B) - w
  // To fulfil this we want to minimize the left hand side: i.e find i which
  // minimizes d(ai,u) - li => dijkstra with initial offset O(nlogn), check
  // each edge O(m) good enough.
  FOR(i, 0, n) {
    vi dAi(n, oo);
    vector<char> vis(n, 0);
    for (auto &q : queries[i]) {
      dAi[q.first] = -q.second;
    }
    // O(nlog(n) + m) Dijkstra
    for (int ppp = 0; ppp < n; ++ppp) {
      int u = -1;
      for (int j = 0; j < n; ++j) {
        if (!vis[j] && (u == -1 || dAi[j] < dAi[u]))
          u = j;
      }
      vis[u] = 1;
      int du = dAi[u];
      if (du == oo)
        break;
      for (auto x : adj[u]) {
        auto e = E[x];
        int v = e.v ^ e.u ^ u;
        if (du + e.w < dAi[v]) {
          dAi[v] = du + e.w;
        }
      }
    }

    // O(m) Check for each edge
    for (int j = 0; j < m; ++j) {
      if (!E[j].useful) {
        if (dAi[E[j].u] <= -d[i][E[j].v] - E[j].w) {
          E[j].useful = 1;
          ++cnt;
        } else if (dAi[E[j].v] <= -d[i][E[j].u] - E[j].w) {
          E[j].useful = 1;
          ++cnt;
        }
      }
    }
  }
  cout << cnt << endl;
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  solve();
}
