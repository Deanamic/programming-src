#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef vector<int> vi;

#define FOR(i, a, b) for (int i = (a); i < int(b); i++)
#define FORD(i, a, b) for (int i = (b)-1; i >= int(a); i--)
#define sz(c) int((c).size())
#define all(c) c.begin(), c.end()
#define debug(x) cerr << #x << ": " << x << endl;
#define ff first
#define ss second

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  int n;
  cin >> n;
  int h[n], b[n];
  for (int i = 0; i < n; ++i) {
    cin >> h[i];
  }
  for (int i = 0; i < n; ++i) {
    cin >> b[i];
  }
  stack<pair<int, ll>> s;
  stack<ll> t;
  t.push(0);
  s.push({0, 0});
  ll curAns;
  for (int i = 0; i < n; ++i) {
    ll maxPrefix = -1e15;
    while (s.top().first > h[i]) {
      maxPrefix = max(maxPrefix, s.top().second);
      s.pop();
      t.pop();
    }

    ll prevBeauty = s.top().second;
    ll withoutPrev = max(maxPrefix + b[i], t.top() + b[i]);
    curAns =
      s.top().first ?
      max(withoutPrev, t.top()) :
      withoutPrev;
    t.push(curAns);
    // The current best is
    // If there is no prevLow, curBeaury(im the lowest), or adding the best
    // Prefix If there is a prevLow, we can consider merging into it

    ll bestPrefix = max(maxPrefix, curAns);
      // The best prefix with height larger then h[i]
    // Is a prefix of prevLow[i] < dp[j] < i = maxPrefix
    // Is that prefix + current beauty
    s.push({h[i], bestPrefix});
    //The cu
    // cerr << h[i] << ' ' << b[i] << ' ';
    // cerr << curAns << ' ';
    // cerr << s.top().second << '\n';
  }
  // cerr << endl;
  cout << curAns << endl;
}

/*
 * dp[i] means best containing i (curAns)
 * prevLow[i] first lower building on the left
 * dp[i+1] = h[i] + max(dp[j]) prevLow[i] < dp[j] < i, dp[prevLow[i]]
 */
