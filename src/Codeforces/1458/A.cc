#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef vector<int> vi;

#define FOR(i, a, b) for (int i = (a); i < int(b); i++)
#define FORD(i, a, b) for (int i = (b)-1; i >= int(a); i--)
#define sz(c) int((c).size())
#define all(c) c.begin(), c.end()
#define debug(x) cerr << #x << ": " << x << endl;

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  int n, m;
  cin >> n >> m;
  ll a[n];
  for (int i = 0; i < n; ++i) {
    cin >> a[i];
  }
  sort(a, a + n);
  ll curG = 0;
  for (int i = 1; i < n; ++i) {
    a[i] -= a[0];
    curG = __gcd(curG, a[i]);
  }
  for (int i = 0; i < m; ++i) {
    ll x;
    cin >> x;
    cout << __gcd(curG, a[0] + x) << ' ';
  }
  cout << endl;
}
