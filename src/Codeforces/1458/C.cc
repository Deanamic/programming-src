#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef vector<int> vi;

#define FOR(i, a, b) for (int i = (a); i < int(b); i++)
#define FORD(i, a, b) for (int i = (b)-1; i >= int(a); i--)
#define sz(c) int((c).size())
#define all(c) c.begin(), c.end()
#define debug(x) cerr << #x << ": " << x << endl;

struct Edge {
  int u, v;
  bool used = 0;
};

vi eularian_path;

void dfs(int u, const vector<vector<int>> &adjList, vector<Edge> &E, vector<int>& nxt) {
  while(nxt[u] < sz(adjList[u])) {
    int i = adjList[u][nxt[u]++];
    if (!E[i].used) {
      E[i].used = true;
      int v = u ^ E[i].u ^ E[i].v;
      dfs(v, adjList, E, nxt);
    }
  }
  eularian_path.push_back(u);
}

void solve() {
  string s;
  cin >> s;
  int cur = 0;
  int mn = 0;
  int mx = 0;
  for (char &c : s) {
    cur += (c == '1' ? 1 : -1);
    mn = min(mn, cur);
    mx = max(mx, cur);
  }
  cur = -mn;
  int n = mx - mn + 1;
  vector<vector<int>> v(n);
  vi nxt(n,0);
  vector<Edge> E;
  for (char &c : s) {
    int nxtPos = cur + (c == '1' ? 1 : -1);
    v[cur].push_back(sz(E));
    v[nxtPos].push_back(sz(E));
    E.push_back({cur, nxtPos, 0});
    cur = nxtPos;
  }
  for (int i = 0; i < n; ++i) {
    sort(v[i].begin(), v[i].end(), [i, &E](auto a, auto b) {
      int va = i ^ E[a].u ^ E[a].v;
      int vb = i ^ E[b].u ^ E[b].v;
      return va < vb;
    });
  }
  eularian_path.clear();
  dfs(-mn, v, E, nxt);
  reverse(all(eularian_path));
  for (int i = 0; i < sz(eularian_path) - 1; ++i) {
    cout << max(eularian_path[i + 1] - eularian_path[i], 0);
  }
  cout << endl;
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  int T;
  cin >> T;
  while (T--) {
    solve();
  }
}
