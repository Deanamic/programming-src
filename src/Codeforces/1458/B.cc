#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef vector<int> vi;

#define FOR(i, a, b) for (int i = (a); i < int(b); i++)
#define FORD(i, a, b) for (int i = (b)-1; i >= int(a); i--)
#define sz(c) int((c).size())
#define all(c) c.begin(), c.end()
#define debug(x) cerr << #x << ": " << x << endl;

struct Cup {
  int a, b;
};
int dp[100 + 1][100 + 1][100 * 100 + 10];

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  int n;
  cin >> n;
  vector<Cup> v(n);
  long double Bsum = 0;
  for (int i = 0; i < n; ++i) {
    cin >> v[i].a >> v[i].b;
    Bsum += v[i].b;
  }
  memset(dp, -1, sizeof(dp));
  dp[0][0][0] = 0;
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j <= i; ++j) {
      for (int k = 0; k < 100 * n; ++k) {
        if (dp[i][j][k] != -1) {
          // if i get the new cup
          dp[i + 1][j + 1][k + v[i].a] =
              max(dp[i + 1][j + 1][k + v[i].a], dp[i][j][k] + v[i].b);
          // if we don't take the cup
          dp[i + 1][j][k] = max(dp[i + 1][j][k], dp[i][j][k]);
        }
      }
    }
  }
  long double ans[n];
  memset(ans, 0, sizeof(ans));
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < 100 * n + 5; ++j) {
      if(dp[n][i+1][j] != -1)
      ans[i] = max(ans[i], min((long double)j, (dp[n][i + 1][j] + Bsum) / 2.0));
    }
  }
  for (auto i : ans)
    cout << i << ' ';
  cout << endl;
}
