#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef vector<int> vi;

#define FOR(i, a, b) for (int i = (a); i < int(b); i++)
#define FORD(i, a, b) for (int i = (b)-1; i >= int(a); i--)
#define sz(c) int((c).size())
#define all(c) c.begin(), c.end()
#define debug(x) cerr << #x << ": " << x << endl;

struct diagonal {
  int x, y;
  int length;
  bool belongs(pair<int, int> p) const {
    if (x > p.first || x + length <= p.first)
      return false;
    if (y > p.second || y + length <= p.second)
      return false;
    return p.first - x == p.second - y;
  }
  bool operator<(const int p) const { return x < p; }
};

bool inDiagonal(const vector<diagonal> &vd, pair<int, int> p) {
  int l = 0, r = sz(vd);
  while(l + 1 < r) {
    int m = (l + r) >> 1;
    if(vd[m].x <= p.first) l = m;
    else r = m;
  }
  return vd[l].belongs(p);
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  int n, m;
  cin >> n >> m;

  map<int, int> vX, vY;
  set<pair<int, int>> s;
  vX[1e9 + 10] = 1e9 + 10;
  vY[1e9 + 10] = 1e9 + 10;
  s.insert({1e9 + 10, 1e9 + 10});

  for (int i = 0; i < n; ++i) {
    int x, y;
    cin >> x >> y;
    s.insert({x, y});
    if (vX.count(x)) {
      vX[x] = min(vX[x], y);
    } else {
      vX[x] = y;
    }

    if (vY.count(y)) {
      vY[y] = min(vY[y], x);
    } else {
      vY[y] = x;
    }
  }
  vector<diagonal> vd;
  int curX = 0, curY = 0;
  while(max(curX, curY) < 1e9 + 3) {
    // The points is lost if it is a shortcut
    if (s.count({curX, curY})) {
      curX++;
      curY++;
    }
    // The point is won if there is a lost point to the left -> x+1
    else if (vY.count(curY) && vY[curY] < curX) {
      curY++;
    }
    // The poitn is won if there is a lost point to the bottom -> y+1
    else if (vX.count(curX) && vX[curX] < curY) {
      curX++;
    }
    // The point is lost if not -> Go to +1 +1 (all diagonal is fine)
    // lower bound should equal upper bound
    else {
      int nextY = (*vY.lower_bound(curY+1)).first;
      int nextX = (*vX.lower_bound(curX+1)).first;
      // cout << "next: " << nextX << ' ' << nextY << endl;
      int l = min(nextY - curY, nextX - curX);
      vd.push_back({curX, curY, l});
      curX += l;
      curY += l;
    }
  }
  for(int i = 0; i < m; ++i) {
    int a, b;
    cin >> a >> b;
    cout << ((s.count({a, b}) || inDiagonal(vd, {a, b})) ? "LOSE\n" : "WIN\n");
  }
}
