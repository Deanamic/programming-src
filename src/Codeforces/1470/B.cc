#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef vector<int> vi;

#define FOR(i, a, b) for (int i = (a); i < int(b); i++)
#define FORD(i, a, b) for (int i = (b)-1; i >= int(a); i--)
#define sz(c) int((c).size())
#define all(c) c.begin(), c.end()
#define debug(x) cerr << #x << ": " << x << endl;

const static int SIEVE_SIZE = 1e6 + 10;
vi sieve(SIEVE_SIZE, 0);
vi primes;

void solve() {
  int n;
  cin >> n;
  vi a(n);
  map<string, int> m;
  int mx0 = 0;
  for (auto i = 0; i < n; ++i) {
    int x;
    cin >> x;
    set<int> oddFactors;
    while(x > 1) {
      int divisor = sieve[x];
      if(oddFactors.count(divisor)) {
        oddFactors.erase(divisor);
      } else {
        oddFactors.insert(divisor);
      }
      x /= sieve[x];
    }
    string s = "empty";
    for(int u : oddFactors) s += to_string(u) + ",";
    m[s] = m[s] + 1;
    mx0 = max(mx0, m[s]);
  }
  int mx1 = 0;
  for(auto key : m) {
    if(key.second%2 == 0 || key.first == "empty") mx1 += key.second;
  }
  int q;
  cin >> q;
  while (q--) {
    ll w;
    cin >> w;
    cout << (w > 0 ? max(mx0, mx1) : mx0) << '\n';
  }
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  for (int i = 2; i < SIEVE_SIZE; ++i) {
    if (sieve[i] == 0) {
      sieve[i] = i;
      primes.push_back(i);
    }
    for (int j = 0;
         j < sz(primes) && primes[j] <= sieve[i] && i * primes[j] < SIEVE_SIZE;
         ++j) {
      sieve[i * primes[j]] = primes[j];
    }
  }
  int t;
  cin >> t;
  while (t--) {
    solve();
  }
}
