#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef vector<int> vi;

#define FOR(i, a, b) for (int i = (a); i < int(b); i++)
#define FORD(i, a, b) for (int i = (b)-1; i >= int(a); i--)
#define sz(c) int((c).size())
#define all(c) c.begin(), c.end()
#define debug(x) cerr << #x << ": " << x << endl;

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  int t;
  cin >> t;
  while(t--){
    int n, m;
    cin >> n >> m;
    vi v(n), c(m);
    for (int i = 0; i < n; ++i) {
      cin >> v[i];
      --v[i];
    }
    for (auto i = 0; i < m; ++i) {
      cin >> c[i];
    }
    sort(all(v));
    ll ans = 0;
    int it = 0;
    for(int i = n-1; i >= 0; --i){
      int curK = v[i];
      if (it < m && curK > it) {
        ans += c[it++];
      } else {
        ans += c[curK];
      }
    }
    cout << ans << '\n';
  }
}
