#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef vector<int> vi;

#define FOR(i, a, b) for (int i = (a); i < int(b); i++)
#define FORD(i, a, b) for (int i = (b)-1; i >= int(a); i--)
#define sz(c) int((c).size())
#define all(c) c.begin(), c.end()
#define debug(x) cerr << #x << ": " << x << endl;

vector<vi> adj;
int leftL[100005], rightL[100005];
vector<pair<int,int>> cur;
vector<pair<int,int>> cuts;

int dfs(int u, int p, bool c = 0) {
  if(c && u != p) {
    cuts.push_back({u + 1, p + 1});
  }
  int k = 0;
  leftL[u] = rightL[u] = u;
  for (int v : adj[u]) {
    if (v != p) {
      auto cut = dfs(v, u, k >= 2);

      if (cut == 0) {
        k++;
        if (k == 1) {
          leftL[u] = leftL[v];
        }
        if (k == 2) {
          rightL[u] = leftL[v];
        }
      }

      if (!c && k == 2) {
        cuts.push_back({u + 1, p + 1});
        c = 1;
      }
    }
  }
  if(c) cur.push_back({leftL[u] + 1, rightL[u] + 1});
  return c;
}

void solve() {
  int n;
  cin >> n;
  adj = vector<vi>(n);
  cur.clear();
  cuts.clear();
  for (int i = 0; i < n - 1; ++i) {
    int x, y;
    cin >> x >> y;
    --x, --y;
    adj[x].push_back(y);
    adj[y].push_back(x);
  }
  dfs(0, 0, 1);
  assert(sz(cuts) + 1 == sz(cur));
  cout << sz(cuts) << '\n';
  for(int i = 0; i < sz(cuts); ++i) {
    cout << cuts[i].first << ' ' << cuts[i].second << ' ' << cur[i].second << ' ' << cur[i+1].first << '\n';
  }
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  int T;
  cin >> T;
  while (T--) {
    solve();
  }
}
