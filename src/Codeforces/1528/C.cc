#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef vector<int> vi;

#define FOR(i, a, b) for (int i = (a); i < int(b); i++)
#define FORD(i, a, b) for (int i = (b)-1; i >= int(a); i--)
#define sz(c) int((c).size())
#define all(c) c.begin(), c.end()
#define debug(x) cerr << #x << ": " << x << endl;

vector<vi> treeS, treeK;
vi eul_path;
const int MAXN = 300005;
int tIn[MAXN], tOut[MAXN];
int reverseT[MAXN*2];
vi parent;
int ans, curAns;
int n;

struct FT {
  vector<int> s;
  FT() {}
  FT(int m) : s(m) {}
  void update(int pos, int dif) { // a[pos] += dif
    for (; pos < sz(s); pos |= pos + 1)
      s[pos] += dif;
  }
  int query(int pos) { // sum of values in [0, pos]
    pos++;
    int res = 0;
    for (; pos > 0; pos &= pos - 1)
      res += s[pos - 1];
    return res;
  }
  int lower_bound(ll sum) { // min pos st sum of [0, pos] >= sum
    // Returns n if no sum is >= sum, or -1 if empty sum is.
    if (sum <= 0)
      return -1;
    int pos = 0;
    for (int pw = 1 << 25; pw; pw >>= 1) {
      if (pos + pw <= sz(s) && s[pos + pw - 1] < sum)
        pos += pw, sum -= s[pos - 1];
    }
    return pos;
  }
} Fenwick;

void preprocess_dfs(int u) {
  tIn[u] = sz(eul_path);
  reverseT[sz(eul_path)] = u;
  eul_path.push_back(u);
  for (int v : treeS[u]) {
    preprocess_dfs(v);
  }
  tOut[u] = sz(eul_path);
  reverseT[sz(eul_path)] = u;
  eul_path.push_back(u);
}

void update(int u, int p = -1) {
  if(p != -1) {
    Fenwick.update(tIn[p], -1);
    Fenwick.update(tOut[p], -1);
    parent[u] = p;
  } else {
    curAns++;
  }
  Fenwick.update(tIn[u], 1);
  Fenwick.update(tOut[u], 1);
  ans = max(curAns, ans);
}

void remove(int u) {
  if (parent[u] != -1) {
    int p = parent[u];
    Fenwick.update(tIn[p], 1);
    Fenwick.update(tOut[p], 1);
  } else {
    curAns--;
  }
  Fenwick.update(tIn[u], -1);
  Fenwick.update(tOut[u], -1);
}

void dfs(int u) {
  // Check if their is a child in the tree, if so continue
  int parity = (Fenwick.query(tOut[u]-1) - Fenwick.query(tIn[u])) > 0;
  if (parity == 0) {
    int p = -1;
    // Check if there is a parent colored
    int prefSum = Fenwick.query(tIn[u]-1);
    //If the prefix sum is odd, it means there is an closed node, it means tOut[x] > tIn[u], but tIn[x] < tIn[u] it means is a parent
    //In a addition, any colored node is closed immediately because we do not have childs and parents colored at the same time
    if(prefSum % 2) {
      p = reverseT[Fenwick.lower_bound(prefSum + 1)];
    }
    update(u, p);
  }
  for (int v : treeK[u]) {
    dfs(v);
  }
  //undo the change
  remove(u);
}

void solve() {
  cin >> n;
  curAns = ans = 0;
  Fenwick = FT(2 * n + 10);
  treeS = vector<vi>(n);
  treeK = vector<vi>(n);
  eul_path.clear();
  parent = vi(n, -1);
  for (int i = 1; i < n; ++i) {
    int x;
    cin >> x;
    --x;
    treeK[x].push_back(i);
  }
  for (int i = 1; i < n; ++i) {
    int x;
    cin >> x;
    --x;
    treeS[x].push_back(i);
  }
  preprocess_dfs(0);
  dfs(0);
  cout << ans << endl;
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  int t;
  cin >> t;
  while (t--) {
    solve();
  }
}
