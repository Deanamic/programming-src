#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef vector<int> vi;

#define FOR(i, a, b) for (int i = (a); i < int(b); i++)
#define FORD(i, a, b) for (int i = (b)-1; i >= int(a); i--)
#define sz(c) int((c).size())
#define all(c) c.begin(), c.end()
#define debug(x) cerr << #x << ": " << x << endl;

const int MAXN = 100010;
int l[MAXN], r[MAXN];
vector<vi> adj;
ll dp1[MAXN], dp2[MAXN];

ll dfs(int u, int p) {
  dp1[u] = dp2[u] = 0;
  for (int v : adj[u]) {
    if(v != p) {
      dfs(v, u);
      dp1[u] += max(abs(l[u] - l[v]) + dp1[v], abs(l[u] - r[v]) + dp2[v]);
      dp2[u] += max(abs(r[u] - l[v]) + dp1[v], abs(r[u] - r[v]) + dp2[v]);
    }
  }
  return max(dp1[u], dp2[u]);
}

void solve() {
  int n;
  cin >> n;
  for(int i = 0; i < n; ++i) {
    cin >> l[i] >> r[i];
  }
  adj = vector<vi> (n);
  for(int i = 1; i < n; ++i) {
    int x, y;
    cin >> x >> y;
    --x, --y;
    adj[x].push_back(y);
    adj[y].push_back(x);
  }
  cout << dfs(0, 0) << endl;
}

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  int T;
  cin >> T;
  while(T--) {
    solve();
  }
}
