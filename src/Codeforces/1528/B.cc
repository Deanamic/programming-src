#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef vector<int> vi;

#define FOR(i, a, b) for (int i = (a); i < int(b); i++)
#define FORD(i, a, b) for (int i = (b)-1; i >= int(a); i--)
#define sz(c) int((c).size())
#define all(c) c.begin(), c.end()
#define debug(x) cerr << #x << ": " << x << endl;

const int MOD = 998244353;
int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  int n;
  cin >> n;
  vector<ll> dp(n+1, 1);
  ll s = 0;
  for (int i = 1; i <= n; i++) {
    for (int j = i + i; j <= n; j += i) {
      dp[j]++;
    }
  }
  for (int i = 1; i <= n; ++i) {
    dp[i] = (dp[i] + s) % MOD;
    s = (s + dp[i]) % MOD;
  }
  cout << dp[n] << endl;
}
