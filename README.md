# Programming-src

| Key | Meaning        |
|-----|----------------|
| A/# | Accepted/Tries |
| W   | Wrong Answer   |
| U   | Upsolved       |
| -   | Unsolved       |
| N   | NotWorth       |

## [CF 1548](./src/Codeforces/1548/)
| A | B | C | D | E |
|---|---|---|---|---|
| - | A | - | - | - |
B: Mathematics gcd problem, relate same module in subarray to gcd


## [CF 1528](./src/Codeforces/1528/)
| A | B | C | D | E |
|---|---|---|---|---|
| A | A | A | - | - |

-------------------------------------

## [CF 1521](./src/Codeforces/1521/)
| A | B | C | D | E |
|---|---|---|---|---|
| N | N | N | U | N |

Div 2 round

-------------------------------------

## [Codejam 2021: Round 2](./src/CodeJam/2021/Round2/)

| A | B | C | D |
|---|---|---|---|
| A | U | U | U |

A: Naive solution fits under constraints
D: MaxFlowMinCost/Hungarian Algorithm

-------------------------------------

## [Codejam 2021: Round 1B](./src/CodeJam/2021/Round1B/)

| A | B | C |
|---|---|---|
| N | A | A |

C: Interactive problem: Maximize expectation with a DP

-------------------------------------

## [Codejam 2021: Round 1A](./src/CodeJam/2021/Round1A/)

| A | B | C   |
|---|---|-----|
| A | A | 2/3 |

-------------------------------------



## [CF 1503](./src/Codeforces/1503/)
| A | B | C | D | E | F |
|---|---|---|---|---|---|
| 1 | 1 | 3 | 3 | - | - |

-------------------------------------

## [CF 1483](./src/Codeforces/1483/)
| A | B | C |   D | E |   F |
|---|---|---|-----|---|-----|
| 7 | 4 | 8 | 10+ | - | 10+ |

Rusty contest after long hiatus  
A: Standard greedy  
B: Standard Implementation  
C: Standard dp, errors due to trying the stack trick for dp  
D: Interesting problem on graphs, dijkstra implementation is too slow for dense graphs!!  
E: Similar ideas to Egg drop problem, based on having a "non-linear" biniary search. Requires a interesting way to calculate the middle point. A dp with the number of guess and failures allowed as state, and the biggest length which we can obtain the answer for.  
F: Hard problem with strings and substrings, Solved with `Aho-corasick`. Using techniques of compression and clever movement through suffix links to avoid TL  

-------------------------------------

## [CF 1479](./src/Codeforces/1479/)
| A | B | C | D | E | 
|---|---|---|---|---|
| N | N | N | A | U |

D: Pending refactor, Mo's algorithm on a tree + SQRT decomposition

-------------------------------------

## [CF 1470](./src/Codeforces/1470)
| A | B | C | D | E | F |
|---|---|---|---|---|---|
| A | A | - | - | - | - |

-------------------------------------

## [CF 1456](./src/Codeforces/1456/)
| A | B | C | D | E | F |
|---|---|---|---|---|---|
| A | A | A | A | U | - |

F: Hard/Long `ad-hoc` problem about `DP`, `xor`

-------------------------------------

## [CF 1458](./src/Codeforces/1458/)
| A | B | C | D | E | F |
|---|---|---|---|---|---|
| A | U | U | - | U | - |
